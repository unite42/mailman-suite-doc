Internationalization(i18n)
==========================

GNU Mailman project uses Weblate_ for translations. If you are interested to
port Mailman to various languages, please head over to Weblate_ and create an
account to get started with translations.

Weblate has very good documentation on how to use it:

    https://docs.weblate.org/en/latest/user/translating.html

Please do not create Merge Requests for translations since it can create merge
conflicts when pulling changes from Weblate and break automation which pulls
and pushes changes between Gitlab_ and Weblate_.

Offline translations
--------------------
If you have existing translated ``.po`` files, or you would prefer to work Offline,
you can download the ``.po`` files from Weblate and upload them through the web
interface when you are done. This helps with keeping the merge request based workflow
to ingest translations from Weblate and still allowing folks to use their preferred
local editing environment.

Please see Weblate's documentation on `downloading and uploading po files`_. 

Integration with Weblate
------------------------

Integration with source control in Gitlab_ and translation project in Weblate_
works using webhooks and some scripts.

Weblate supports webhooks for notifications when there are changes to the
source control. This allows pulling changes to source strings from Gitlab by
adding a webhook notification in Gitlab.

Translations are converted to Git commits in Weblate and it is configured to
send back those commits as Merge Requests, every 24 hours, to Mailman's Gitlab
projects. Each commit corresponds to a single Author and Language. We do not 
squash these commits so as to retain the original commits with Authorship info.


.. _Weblate: https://hosted.weblate.org/projects/gnu-mailman/
.. _Gitlab: https://gitlab.com/mailman/
.. _`downloading and uploading po files`: https://docs.weblate.org/en/latest/user/files.html
